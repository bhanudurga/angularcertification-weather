import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeatherService } from '../services/weather.service';
import * as moment from 'moment'
@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {
  zipcode: any;
  forecast: any = [];
  city: any;
  constructor(private route: ActivatedRoute, private weatherService: WeatherService, private router: Router) {
    this.zipcode = this.route.snapshot.params.code;
    this.getForecast();
  }
  ngOnInit(): void {
  }
  getForecast() {
    this.weatherService.getLocationForecast(this.zipcode).subscribe((res: any) => {
      if (res.cod == 200) {
        this.forecast = res.list;
        this.city = res.city.name;
      }
    }, err => {
      console.log(err, 'err');
    });
  }
  weatherImage(type) {
    if (type == 'Thunderstorm' || type == 'Rain' || type == 'Drizzle') {
      return 'assets/rain.png'
    }
    else if (type == 'Clear') {
      return 'assets/sun.png'
    }
    else if (type == 'Clouds') {
      return 'assets/clouds.png'
    }
    else if (type == 'Snow') {
      return 'assets/snow.png'
    }
  }
  getDay(date) {
    return moment(date, 'YYYY-MM-DD').format('dddd')
  }
  back() {
    this.router.navigateByUrl('/')
  }
}
