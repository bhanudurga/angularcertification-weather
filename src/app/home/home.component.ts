import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WeatherService } from '../services/weather.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  location: any = '';
  allLocations: any = [];
  errMsg: any = '';
  constructor(private weatherService: WeatherService, private router: Router) {
    if (localStorage.getItem('locations')) {
      this.allLocations = JSON.parse(localStorage.getItem('locations'));
      this.refreshWeatherDetails();
    }
  }

  ngOnInit(): void {
  }
  refreshWeatherDetails() {
    for (let i = 0; i < this.allLocations.length; i++) {
      this.weatherDetails(this.allLocations[i].zipcode)
    }
  }
  addLocation() {
    console.log(this.location, 'location');
    if (this.location != '') {
      if (Array.isArray(this.allLocations)) {
        let index = this.allLocations.findIndex((e) => e.zipcode === this.location)
        console.log(index, 'index');
        if (index == -1) {
          this.weatherDetails(this.location);
        }
      }
      else {
        this.allLocations = []
        this.weatherDetails(this.location);
      }
    }
  }
  weatherDetails(location) {
    let weather: any = {
      'name': '',
      'currentCondition': '',
      'temperature': '',
      'maxTemp': '',
      'minTemp': '',
    }
    this.weatherService.getLocationWeather(location).subscribe((res: any) => {
      if (res.cod == 200) {
        this.errMsg = '';
        weather.name = res.name;
        weather.currentCondition = res.weather[0].main;
        weather.temperature = res.main.temp;
        weather.maxTemp = res.main.temp_max;
        weather.minTemp = res.main.temp_min;
        if (res.weather[0].main == 'Thunderstorm' || res.weather[0].main == 'Rain' || res.weather[0].main == 'Drizzle') {
          weather.image = 'assets/rain.png'
        }
        else if (res.weather[0].main == 'Clear') {
          weather.image = 'assets/sun.png'
        }
        else if (res.weather[0].main == 'Clouds' || res.weather[0].main == 'Haze') {
          weather.image = 'assets/clouds.png'
        }
        else if (res.weather[0].main == 'Snow') {
          weather.image = 'assets/snow.png'
        }
        let index = this.allLocations.findIndex((e) => e.zipcode == location)
        if (index != -1) {
          this.allLocations[index].details = weather;
        }
        else {
          let location: any = { 'zipcode': this.location, 'details': weather };
          this.allLocations.push(location);
        }
        localStorage.setItem('locations', JSON.stringify(this.allLocations));
      }
      else {
        this.errMsg = 'City not found';
      }
    }, err => {
      console.log(err, 'err');
      this.errMsg = 'City not found';
    });
  }
  close(i) {
    this.allLocations.splice(i, 1);
    localStorage.setItem('locations', JSON.stringify(this.allLocations));
  }
  forecast(i) {
    this.router.navigateByUrl('forecast/' + this.allLocations[i].zipcode)
  }
}
