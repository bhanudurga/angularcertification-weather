import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient: HttpClient) { }
  getLocationWeather(location) {
    return this.httpClient.get("https://api.openweathermap.org/data/2.5/weather?zip=" + location + ",in&APPID=5a4b2d457ecbef9eb2a71e480b947604&units=imperial");
  }
  getLocationForecast(location) {
    return this.httpClient.get("https://api.openweathermap.org/data/2.5/forecast?zip=" + location + ",in&APPID=5a4b2d457ecbef9eb2a71e480b947604&units=imperial");
  }
}